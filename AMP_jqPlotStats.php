<?php
/*
 *      jqPlotStats is a plugin for fluxBB 1.5.x. it display forum's statistics
 *      with the exelente jqplot framework <http://www.jqplot.com/>.
 *
 *        - Plugin homepage: <http://jonas.tuxfamily.org/wiki/fluxbb/jqPlotStats>
 *        - Author : Jonas Fourquier (homepage: <jonas.tuxfamily.org>)
 *
 *      This plugin it based on the plugin "statistique" by fanf73 from fluxbb.fr
 *      "statistique" availabe here http://fluxbb.fr/mods/mods.php?mid=174
 *
 *      This is free software; you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation; either version 2 of the License, or
 *      (at your option) any later version.
 *
 *      This program is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 *
 *      You should have received a copy of the GNU General Public License
 *      along with this program; if not, write to the Free Software
 *      Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 *      MA 02110-1301, USA.
 */


if (!defined('PUN'))
    exit;

define('PUN_PLUGIN_LOADED', 1);
generate_admin_menu($plugin);


function aff_top_users($result,$id) {

    global $db;

    $array4jqplot = array();
    while($rep = $db->fetch_assoc($result)) {
        $array4jqplot[] = '[\''.$rep['poster'].'\','.$rep['num_posts'].']';
    }
    if (empty($array4jqplot)) {
        echo '<p>Aucune donn�e pour cette p�riode</p>';
    } else {
        ?>
        <div id="chart_<?php echo $id ?>" style="height:300px;width:100%; "></div>
        <script language='javascript' type='text/javascript'>
            var line_<?php echo $id ?> = [<?php echo join(',',$array4jqplot) ?>];
            $.jqplot.config.enablePlugins = true;
            $.jqplot('chart_<?php echo $id ?>', [line_<?php echo $id ?>], {
                series:[{renderer:$.jqplot.BarRenderer}],
                axesDefaults: {
                    tickRenderer: $.jqplot.CanvasAxisTickRenderer,
                    tickOptions: {
                        fontSize: '10pt'
                      }
                  },
                axes: {
                    xaxis: {
                        renderer: $.jqplot.CategoryAxisRenderer,
                        tickOptions: {angle: -45}
                      },
                  }
              });
        </script>
        <?php
    }
}


function aff_top_topics($result,$id) {

    global $db;

    $array4jqplot = array();
    while($rep = $db->fetch_assoc($result)) {
        $array4jqplot[] = '[\''.$rep['poster'].'\','.$rep['num_topics'].']';
    }

    if (empty($array4jqplot)) {
        echo '<p>Aucune donn�e pour cette p�riode</p>';
    } else {
        ?>
        <div id="chart_<?php echo $id ?>" style="height:300px;width:100%; "></div>
        <script language='javascript' type='text/javascript'>
            var line_<?php echo $id ?> = [<?php echo join(',',$array4jqplot) ?>];
            $.jqplot.config.enablePlugins = true;
            $.jqplot('chart_<?php echo $id ?>', [line_<?php echo $id ?>], {
                series:[{renderer:$.jqplot.BarRenderer}],
                axesDefaults: {
                    tickRenderer: $.jqplot.CanvasAxisTickRenderer,
                    tickOptions: {
                        fontSize: '10pt'
                      }
                  },
                axes: {
                    xaxis: {
                        renderer: $.jqplot.CategoryAxisRenderer,
                        tickOptions: {angle: -45}
                      },
                  }
              });
        </script>
        <?php
    }
}


function aff_records($result, $type) {

    global $db;

    $array4jqplot = array();
    while($rep = $db->fetch_assoc($result)) {
        $array4jqplot[] = '[\''.strftime('%d/%m/%Y',$rep['posted']).'\','.$rep['num_type'].']';
    }

    if (empty($array4jqplot)) {
        echo '<p>Aucune donn�e pour cette p�riode</p>';
    } else {
        ?>
        <div id="chart_<?php echo $type ?>" style="height:300px;width:100%; "></div>
        <script language='javascript' type='text/javascript'>
            var line_<?php echo $type ?> = [<?php echo join(',',$array4jqplot) ?>];
            $.jqplot.config.enablePlugins = true;
            $.jqplot('chart_<?php echo $type ?>', [line_<?php echo $type ?>], {
                series:[{renderer:$.jqplot.BarRenderer}],
                axesDefaults: {
                    tickRenderer: $.jqplot.CanvasAxisTickRenderer,
                    tickOptions: {
                        fontSize: '10pt'
                      }
                  },
                axes: {
                    xaxis: {
                        renderer: $.jqplot.CategoryAxisRenderer,
                        tickOptions: {angle: -45}
                      },
                  }
              });
        </script>
        <?php
    }
}

?>
        <div class="block">
            <h2><span>Statistiques du forum</span></h2>
            <div class="box">
                <div class="inbox">
                    <p>Conna�tre certaines statistiques sur votre forum :
                    <p><a href='admin_loader.php?plugin=AMP_jqPlotStats.php&evolution'>Evolution :</a> voir l'�volution du nombre de messages, de discussions et de membres.</p>
                    <p><a href='admin_loader.php?plugin=AMP_jqPlotStats.php&top'>Meilleurs posteurs :</a> top des meilleurs posteurs du jour, de la semaine, du mois, de l'ann�e et depuis le d�but.</p>
                    <p><a href='admin_loader.php?plugin=AMP_jqPlotStats.php&top_crea'>Meilleurs cr�ateurs :</a> top des plus grands cr�ateurs de topics de la semaine, du mois, de l'ann�e et depuis le d�but.</p>
                    <p><a href='admin_loader.php?plugin=AMP_jqPlotStats.php&record'>Records :</a> conna�tres les jours ayant eu le plus de messages post�s, de discussions cr��es et d'inscriptions.</p>
                    <p><a href='admin_loader.php?plugin=AMP_jqPlotStats.php&repartition'>R�partition :</a> r�partition des messages / topics / membres en fonction du mois de l'ann�e, du jour de la semaine ou de l'heure (ne prends pas en compte les fuseaux horaires des membres).</p>
                </div>
            </div>
        </div>
<?php
if(isset($_GET['evolution'])) {
?>
        <div class="block">
            <h2 class="block2">Evolution du forum</span></h2>
            <div class="box">
                <div class="inbox">
                    <div class="infldset">
                        <?php
                        $array4jqplot = array(
                                'mess' => array(),
                                'topic' => array(),
                                'users' => array()
                            );
                        $min = array();
                        $max = array();
                        $tot_mess = 0;
                        $tot_topic = 0;
                        $tot_users = 0;

                        //post
                        $result = $db->query('SELECT count(*) AS num_type, FROM_UNIXTIME(posted, "%Y/%m") AS type FROM '. $db->prefix .'posts GROUP BY type ASC', true)or error('Database error', __FILE__, __LINE__, $db->error());
                        $sum = 0;
                        while($rep = $db->fetch_assoc($result)) {
                            $sum += $rep['num_type'];
                            if (!isset($min['mess']))
                                $min['mess'] = array('x'=>$rep['type'].'/O1', 'y'=>$sum);
                            $max['mess'] = array('x'=>$rep['type'].'/O1', 'y'=>$sum);
                            $array4jqplot['mess'][] = '[\''.$rep['type'].'/01\','.$sum.',\'pouet\']';
                        }

                        //topic
                        $result = $db->query('SELECT count(*) AS num_type, FROM_UNIXTIME(posted, "%Y/%m") AS type FROM '. $db->prefix .'topics GROUP BY type ASC', true)or error('Database error', __FILE__, __LINE__, $db->error());
                        $sum = 0;
                        while($rep = $db->fetch_assoc($result)) {
                            $sum += $rep['num_type'];
                            if (!isset($min['topic']))
                                $min['topic'] = array('x'=>$rep['type'].'/O1', 'y'=>$sum);
                            $max['topic'] = array('x'=>$rep['type'].'/O1', 'y'=>$sum);
                            $array4jqplot['topic'][] = '[\''.$rep['type'].'/01\','.$sum.',\'pouet\']';
                        }

                        //users
                        $result = $db->query('SELECT count(*) AS num_type, FROM_UNIXTIME(registered, "%Y/%m") AS type FROM '. $db->prefix .'users GROUP BY type ASC', true)or error('Database error', __FILE__, __LINE__, $db->error());
                        $sum = 0;
                        while($rep = $db->fetch_assoc($result)) {
                            $sum += $rep['num_type'];
                            if($rep['type'] != '1970/01') {
                                if (!isset($min['users']))
                                    $min['users'] = array('x'=>$rep['type'].'/O1', 'y'=>$sum);
                                $max['users'] = array('x'=>$rep['type'].'/O1', 'y'=>$sum);
                                $array4jqplot['users'][] = '[\''.$rep['type'].'/01\','.$sum.',\'pouet\']';
                            }
                        }
                        ?>

                        <!--[if lt IE 9]><script language="javascript" type="text/javascript" src="js/jqplot/excanvas.js"></script><![endif]-->
                        <script language="javascript" type="text/javascript" src="js/jqplot/jquery.min.js"></script>
                        <script language="javascript" type="text/javascript" src="js/jqplot/jquery.jqplot.min.js"></script>
                        <script type="text/javascript" language="javascript" src="js/jqplot/plugins/jqplot.dateAxisRenderer.js"></script>
                        <script type="text/javascript" language="javascript" src="js/jqplot/plugins/jqplot.highlighter.min.js"></script>
                        <script type="text/javascript" language="javascript" src="js/jqplot/plugins/jqplot.canvasTextRenderer.js"></script>
                        <script type="text/javascript" language="javascript" src="js/jqplot/plugins/jqplot.canvasAxisLabelRenderer.js"></script>
                        <script type="text/javascript" language="javascript" src="js/jqplot/plugins/jqplot.canvasAxisTickRenderer.js"></script>
                        <link rel="stylesheet" type="text/css" href="js/jqplot/jquery.jqplot.css" />
                        <div id="chart_mess" style="height:400px;width:100%; "></div>
                        <div id="chart_topic" style="height:400px;width:100%; "></div>
                        <div id="chart_users" style="height:400px;width:100%; "></div>

                        <script language='javascript' type='text/javascript'>
                            $.jqplot.config.enablePlugins = true;

                            <?php
                        foreach (array('topic'=>'sujets','mess'=>'messages','users'=>'Utilisateurs') as $key=>$title) {
                            ?>
                            chart_<?php echo $key ?> = [<?php echo join(',',$array4jqplot[$key]) ?>];
                            $.jqplot('chart_<?php echo $key ?>', [chart_<?php echo $key ?>], {
                              title:'Nombre de <?php echo $title ?>',
                              axes: {
                                xaxis: {
                                  renderer: $.jqplot.DateAxisRenderer,
                                  tickRenderer: $.jqplot.CanvasAxisTickRenderer,
                                  tickOptions: {
                                      formatString: '%b %y'
                                    },
                                },
                                yaxis: {
                                  min:<?php echo $min[$key]['y'] ?>,
                                  max:<?php echo 1.1*$max[$key]['y'] ?>,
                                  tickOptions: {
                                      formatString: '%i'
                                    },
                                },
                                highlighter: {
                                  show: true,
                                  sizeAdjust: 7.5,
                                  useAxesFormatters: true
                                }
                              }
                            });
                            <?php
                        }
                            ?>
                        </script>
                    </div>
                </div>
            </div>
        </div>
<?php
} else if(isset($_GET['record'])) {
?>
        <!--[if lt IE 9]><script language="javascript" type="text/javascript" src="js/jqplot/excanvas.js"></script><![endif]-->
        <script type="text/javascript" language="javascript" src="js/jqplot/jquery.min.js"></script>
        <script type="text/javascript" language="javascript" src="js/jqplot/jquery.jqplot.min.js"></script>
        <script type="text/javascript" language="javascript" src="js/jqplot/plugins/jqplot.barRenderer.js"></script>
        <script type="text/javascript" language="javascript" src="js/jqplot/plugins/jqplot.pointLabels.js"></script>
        <script type="text/javascript" language="javascript" src="js/jqplot/plugins/jqplot.categoryAxisRenderer.js"></script>
        <script type="text/javascript" language="javascript" src="js/jqplot/plugins/jqplot.canvasTextRenderer.js"></script>
        <script type="text/javascript" language="javascript" src="js/jqplot/plugins/jqplot.canvasAxisLabelRenderer.js"></script>
        <script type="text/javascript" language="javascript" src="js/jqplot/plugins/jqplot.canvasAxisTickRenderer.js"></script>

        <div class="block">
            <h2 class="block2"><span>Journ�es comptabilisant le plus de nouveaux messages</span></h2>
            <div class="box">
                <div class="inbox">
                    <div class="infldset">
                        <?php
                        $result = $db->query('SELECT count(*) AS num_type, posted FROM '. $db->prefix .'posts GROUP BY FROM_UNIXTIME(posted, "%d-%m-%Y") ORDER BY num_type DESC, posted DESC LIMIT 20', true)or error('Database error', __FILE__, __LINE__, $db->error());
                        aff_records($result, 'messages');
                        ?>
                    </div>
                </div>
            </div>
        </div>

        <div class="block">
            <h2 class="block2"><span>Journ�es comptabilisant le plus de nouvelles discussions</span></h2>
            <div class="box">
                <div class="inbox">
                    <div class="infldset">
                        <?php
                        $result = $db->query('SELECT count(*) AS num_type, posted FROM '. $db->prefix .'topics GROUP BY FROM_UNIXTIME(posted, "%d-%m-%Y") ORDER BY num_type DESC, posted DESC LIMIT 20', true)or error('Database error', __FILE__, __LINE__, $db->error());

                        aff_records($result, 'topics');
                        ?>

                    </div>
                </div>
            </div>
        </div>

        <div class="block">
            <h2 class="block2"><span>Journ�es comptabilisant le plus de nouveaux membres</span></h2>
            <div class="box">
                <div class="inbox">
                    <div class="infldset">
                        <?php
                        $result = $db->query('SELECT count(*) AS num_type, registered AS posted FROM '. $db->prefix .'users GROUP BY FROM_UNIXTIME(registered, "%d-%m-%Y") ORDER BY num_type DESC, registered LIMIT 20', true)or error('Database error', __FILE__, __LINE__, $db->error());
                        aff_records($result, 'membres');
                        ?>

                    </div>
                </div>
            </div>
        </div>

<?php
} else if(isset($_GET['repartition'])) {

    $format_unix = array("%b", "%a", "%H");
    $format_unix4sort = array("%m", "%w", "%H");
    $titre_h2 = array("R�parition en fonction du mois de l'ann�e", "R�parition des messages en fonction du jour de la semaine", "R�parition des messages en fonction de l'heure");
    $tab_val = array('mois', 'jour', 'heure');
?>
        <!--[if lt IE 9]><script language="javascript" type="text/javascript" src="js/jqplot/excanvas.js"></script><![endif]-->
        <script language="javascript" type="text/javascript" src="js/jqplot/jquery.min.js"></script>
        <script language="javascript" type="text/javascript" src="js/jqplot/jquery.jqplot.min.js"></script>
        <script type="text/javascript" language="javascript" src="js/jqplot/plugins/jqplot.barRenderer.js"></script>
        <script type="text/javascript" language="javascript" src="js/jqplot/plugins/jqplot.pointLabels.js"></script>
        <script type="text/javascript" language="javascript" src="js/jqplot/plugins/jqplot.categoryAxisRenderer.js"></script>
        <script type="text/javascript" language="javascript" src="js/jqplot/plugins/jqplot.canvasTextRenderer.js"></script>
        <script type="text/javascript" language="javascript" src="js/jqplot/plugins/jqplot.canvasAxisLabelRenderer.js"></script>
        <script type="text/javascript" language="javascript" src="js/jqplot/plugins/jqplot.canvasAxisTickRenderer.js"></script>
<?php
    foreach($tab_val AS $id=>$lib) {
?>
        <div class="block">
            <h2 class="block2"><span><?php echo $titre_h2[$id]; ?></span></h2>
            <div class="box">
                <div class="inbox">
                    <div class="infldset">
                        <?php

                        //posts
                        $result = $db->query('SELECT count(*) AS num_type, FROM_UNIXTIME(posted, "'.$format_unix[$id].'") AS type, FROM_UNIXTIME(posted, "'.$format_unix4sort[$id].'") AS sort FROM '. $db->prefix .'posts GROUP BY type ORDER BY sort DESC', true)or error('Database error', __FILE__, __LINE__, $db->error());
                        $i = 0;
                        $posts = array();
                        while($reponse = $db->fetch_assoc($result)) {
                            $i++;
                            $posts[] = '['.$reponse['num_type'].',\''.$reponse['type'].'\',\''.$reponse['num_type'].' ('.$i.'�)\']';
                        }

                        //topics
                        $result = $db->query('SELECT count(*) AS num_type, FROM_UNIXTIME(posted, "'.$format_unix[$id].'") AS type, FROM_UNIXTIME(posted, "'.$format_unix4sort[$id].'") AS sort FROM '. $db->prefix .'topics GROUP BY type ORDER BY sort DESC', true)or error('Database error', __FILE__, __LINE__, $db->error());
                        $i = 0;
                        $topics = array();
                        while($reponse = $db->fetch_assoc($result)) {
                            $i++;
                            $topics[] = '['.$reponse['num_type'].',\''.$reponse['type'].'\',\''.$reponse['num_type'].' ('.$i.'�)\']';
                        }

                        //users
                        $result = $db->query('SELECT count(*) AS num_type, FROM_UNIXTIME(registered, "'.$format_unix[$id].'") AS type, FROM_UNIXTIME(registered, "'.$format_unix4sort[$id].'") AS sort FROM '. $db->prefix .'users GROUP BY type ORDER BY sort DESC', true)or error('Database error', __FILE__, __LINE__, $db->error());
                        $i = 0;
                        $users = array();
                        while($reponse = $db->fetch_assoc($result)) {
                            $i++;
                            $users[] = '['.$reponse['num_type'].',\''.$reponse['type'].'\',\''.$reponse['num_type'].' ('.$i.'�)\']';
                        }


                        ?>
                        <div style="position:relative">
                            <div id="chart_rep_<?php echo $id ?>" style="height:<?php echo count($users)*50 ?>px;width:100%; "></div>
                            <p style="text-align:right; position:absolute; top:10px; right:20px; width: auto">
                                <span style="color:#E5AE24">Nombre d'utilisateurs</span><br />
                                <span style="color:#00BF0F">Nombre de sujets</span><br />
                                <span style="color:#53B5C7">Nombre de message</span>
                            </p>
                        </div>
                        <script language='javascript' type='text/javascript'>
                            $.jqplot.config.enablePlugins = true;
                            $.jqplot('chart_rep_<?php echo $id ?>', [
                                        [<?php echo join(',',$posts) ?>],
                                        [<?php echo join(',',$topics) ?>],
                                        [<?php echo join(',',$users) ?>]
                                      ],
                                    {
                                        series:[
                                            {color:'#53B5C7'},
                                            {color:'#00BF0F'},
                                            {color:'#E5AE24'}
                                          ],
                                        seriesDefaults: {
                                            renderer:$.jqplot.BarRenderer,
                                            rendererOptions: {
                                                barDirection: 'horizontal'
                                              },
                                          },
                                        axes: {
                                            yaxis: {
                                                    renderer: $.jqplot.CategoryAxisRenderer
                                              }
                                          },
                                        /*
                                        //FIXME : css legend en conflif avec css fluxBB
                                        legend: {
                                            show: true,
                                            //placement: 'outsideGrid'
                                          },
                                        */
                                        }
                                );
                        </script>
                    </div>
                </div>
            </div>
        </div>
<?php
    }
} else if(isset($_GET['top'])) {

    $format_date = array(date('Y-m-d'), date('Y-W'), date('Y-m'), date('Y'));
    $format_unix = array("%Y-%m-%d", "%Y-%u", "%Y-%m", "%Y");
    $titre_h2 = array("Meilleurs posteurs de la journ�e", "Meilleurs posteurs de la semaine", "Meilleurs posteurs du mois", "Meilleurs posteurs de l'ann�e");

    $j = 0;
    while(isset($format_date[$j])) {
    ?>

        <!--[if lt IE 9]><script language="javascript" type="text/javascript" src="js/jqplot/excanvas.js"></script><![endif]-->
        <script language="javascript" type="text/javascript" src="js/jqplot/jquery.min.js"></script>
        <script language="javascript" type="text/javascript" src="js/jqplot/jquery.jqplot.min.js"></script>
        <script type="text/javascript" language="javascript" src="js/jqplot/plugins/jqplot.barRenderer.js"></script>
        <script type="text/javascript" language="javascript" src="js/jqplot/plugins/jqplot.pointLabels.js"></script>
        <script type="text/javascript" language="javascript" src="js/jqplot/plugins/jqplot.categoryAxisRenderer.js"></script>
        <script type="text/javascript" language="javascript" src="js/jqplot/plugins/jqplot.canvasTextRenderer.js"></script>
        <script type="text/javascript" language="javascript" src="js/jqplot/plugins/jqplot.canvasAxisLabelRenderer.js"></script>
        <script type="text/javascript" language="javascript" src="js/jqplot/plugins/jqplot.canvasAxisTickRenderer.js"></script>

        <div class="block">
            <h2 class="block2"><span><?php echo $titre_h2[$j] ;?></span></h2>
            <div class="box">
                <div class="inbox">
                    <div class="infldset">
                        <?php
                        $result = $db->query('SELECT count(*) AS num_posts, poster, poster_id FROM '. $db->prefix .'posts WHERE FROM_UNIXTIME(posted, "'.$format_unix[$j].'") = \''.$format_date[$j].'\' GROUP BY poster_id ORDER BY num_posts DESC, poster LIMIT 20', true)or error('Database error', __FILE__, __LINE__, $db->error());
                        aff_top_users($result,$j);
                        ?>
                    </div>
                </div>
            </div>
        </div>
                <?php
                $j++;
    }
    ?>
        <div class="block">
            <h2 class="block2"><span>Meilleurs posteurs depuis le d�but</span></h2>
            <div class="box">
                <div class="inbox">
                    <div class="infldset">
                        <?php
                        $result = $db->query('SELECT id AS poster_id, username AS poster, num_posts FROM '. $db->prefix .'users ORDER BY num_posts DESC LIMIT 20', true)or error('Database error', __FILE__, __LINE__, $db->error());
                        aff_top_users($result,$j+1);
                        ?>
                    </div>
                </div>
            </div>
        </div>
<?php
} else if(isset($_GET['top_crea'])) {

    $format_date = array(date('Y-W'), date('Y-m'), date('Y'));
    $format_unix = array("%Y-%u", "%Y-%m", "%Y");
    $titre_h2 = array("Meilleurs cr�ateurs de topics de la semaine", "Meilleurs cr�ateurs de topics du mois", "Meilleurs cr�ateurs de topics de l'ann�e");

    $j = 0;
    while(isset($format_date[$j])) {
?>

        <!--[if lt IE 9]><script language="javascript" type="text/javascript" src="js/jqplot/excanvas.js"></script><![endif]-->
        <script language="javascript" type="text/javascript" src="js/jqplot/jquery.min.js"></script>
        <script language="javascript" type="text/javascript" src="js/jqplot/jquery.jqplot.min.js"></script>
        <script type="text/javascript" language="javascript" src="js/jqplot/plugins/jqplot.barRenderer.js"></script>
        <script type="text/javascript" language="javascript" src="js/jqplot/plugins/jqplot.pointLabels.js"></script>
        <script type="text/javascript" language="javascript" src="js/jqplot/plugins/jqplot.categoryAxisRenderer.js"></script>
        <script type="text/javascript" language="javascript" src="js/jqplot/plugins/jqplot.canvasTextRenderer.js"></script>
        <script type="text/javascript" language="javascript" src="js/jqplot/plugins/jqplot.canvasAxisLabelRenderer.js"></script>
        <script type="text/javascript" language="javascript" src="js/jqplot/plugins/jqplot.canvasAxisTickRenderer.js"></script>

        <div class="block">
            <h2 class="block2"><span><?php echo $titre_h2[$j] ;?></span></h2>
            <div class="box">
                <div class="inbox">
                    <div class="infldset">
                        <?php
                        $result = $db->query('SELECT COUNT(*) AS num_topics, poster FROM '. $db->prefix .'topics WHERE FROM_UNIXTIME(posted, "'.$format_unix[$j].'") = \''.$format_date[$j].'\' GROUP BY poster ORDER BY num_topics DESC LIMIT 20', true)or error('Database error', __FILE__, __LINE__, $db->error());
                        aff_top_topics($result,$j);
                        ?>
                    </div>
                </div>
            </div>
        </div>
<?php
        $j++;
    }
?>
        <div class="block">
            <h2 class="block2"><span>Meilleurs cr�ateurs de topics depuis le d�but</span></h2>
            <div class="box">
                <div class="inbox">
                    <div class="infldset">
                        <?php
                        $result = $db->query('SELECT COUNT(*) AS num_topics, poster FROM '. $db->prefix .'topics GROUP BY poster ORDER BY num_topics DESC LIMIT 20', true)or error('Database error', __FILE__, __LINE__, $db->error());
                        aff_top_topics($result,$j);
                        ?>
                    </div>
                </div>
            </div>
        </div>
<?php
}
?>
        <div class="block">
            <div class="box">
                <div class="inbox">
                    <p>Graphique propuls� par <a href="http://www.jqplot.com/">jqPlot</a></p>
                </div>

            </div>
        </div>

