Jqplotstats For Fluxbb
=======

Affiche les statistiques fluxBB sous formes de graphiques

![jqplotstat](https://gitlab.com/snouf/jqplotstats-for-fluxbb/uploads/cde5735643d730d4700f56a8e44a26b1/jqplotstat.png)

Installation
-----
  - Téléchargez jQplot <https://bitbucket.org/cleonello/jqplot/downloads/> copier 
    le contenu de l'archive dans le dossier ''/fluxbb/js/jqplot/''
  - Téléchargez jqPlotStats pour fluxBB <https://gitlab.com/snouf/jqplotstats-for-fluxbb/repository/archive.zip?ref=master>
    et copiez ''AMP_jqPlotStats.php'' dans ''/fluxbb/plugins/''

L'accès au statistiques ce fait par le menu latéral de l'interface d'administration de votre forum. Ces statistiques sont accessibles aux modérateurs et administrateurs.

Plus d'infos : <http://jonas.tuxfamily.org/wiki/fluxbb/jqplotstats>